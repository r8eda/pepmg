package it.r8.webapp.pepmg.accauntingdata;

import java.util.List;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.inject.Inject;

import it.r8.webapp.pepmg.core.Repository;

@ManagedBean
@ApplicationScoped
public class AccauntingDataListController {
	
private String test;
	
	@Inject
	private Repository<Integer,AccauntingData> accauntingDataRepository;

	public String getTest() {
		return test;
	}

	public void setTest(String test) {
		this.test = test;
	}

	public List<AccauntingData> getFreeLancers() {
		return accauntingDataRepository.getAll();
	}

}
