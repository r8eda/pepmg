package it.r8.webapp.pepmg.accauntingdata;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Singleton;
import javax.enterprise.context.Dependent;

import it.r8.webapp.pepmg.core.DbConnection;
import it.r8.webapp.pepmg.core.Repository;
import it.r8.webapp.pepmg.accauntingdata.AccauntingData;

@Dependent
@Singleton
public class AccauntingDataRepository extends DbConnection implements Repository<Integer, AccauntingData>{
	
	public void add(AccauntingData accauntingData) {
		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			String sql = String.format("INSERT INTO accauntingdata (amount, orderDate) " + 
										  "VALUES ('%f', '%s')", accauntingData.getAmount(), accauntingData.getOrderDate() ); 
			statement.executeUpdate(sql);

		} catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
		}
	}

	public List<AccauntingData> getAll() {
		List<AccauntingData> accauntingDatas = new ArrayList<AccauntingData>();

		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT * FROM accauntingdata");

			while (resultSet.next()) {
				AccauntingData accauntingData = new AccauntingData();
				accauntingData.setId(resultSet.getInt("id"));
				accauntingData.setIdCompany(resultSet.getInt("idCompany"));
				accauntingData.setIdfreeLancer(resultSet.getInt("idFreeLancer"));
				accauntingData.setAmount(resultSet.getDouble("amount"));
				accauntingData.setOrderDate(resultSet.getTimestamp("orderDate"));
				
				accauntingDatas.add(accauntingData);
			}

		} catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
		}

		return accauntingDatas;
	}
	
	public void update(Integer id, AccauntingData accauntingData) {
		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			String sql = String.format("UPDATE accauntingdata a" +
			                           "  SET amount = '%f'," +
									   "	  orderDate = '%s' " +
									   "WHERE a.id = %d", accauntingData.getAmount(), accauntingData.getOrderDate() , id);
			
			statement.executeUpdate(sql);
			
		} catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
		}
	}
	
	public void delete(Integer id) {
		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			String sql = String.format("DELETE FROM accauntingdata WHERE id = %d", id);
			
			int result = statement.executeUpdate(sql);
			System.out.println(result);
			
		} catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
		}
	}
	
	public AccauntingData getById(Integer id) {
		AccauntingData accauntingData = null;

		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			String sql = String.format("SELECT * FROM accauntingdata a WHERE a.id = '%s'", id);
			
			ResultSet resultSet = statement.executeQuery(sql);
			
			if (resultSet.first()) {
				accauntingData = new AccauntingData();
				accauntingData.setId(resultSet.getInt("id"));
				accauntingData.setIdCompany(resultSet.getInt("idCompany"));
				accauntingData.setIdfreeLancer(resultSet.getInt("idFreeLancer"));
				accauntingData.setAmount(resultSet.getDouble("amount"));
				accauntingData.setOrderDate(resultSet.getTimestamp("orderDate"));
			}
			
		} catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
		}

		return accauntingData;
	}
}
