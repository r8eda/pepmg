package it.r8.webapp.pepmg.accauntingdata;

import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import it.r8.webapp.pepmg.core.Repository;

@ManagedBean
@RequestScoped
public class AccauntingDataAddController {
	
	private AccauntingData accauntingData;

	private Integer accauntingDataIdToUpdate;

	@Inject
	private Repository<Integer, AccauntingData> accauntingDataRepository;

	@PostConstruct
	public void init() {
		Map<String, String> parameters = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();


		if (parameters.get("id") != null) {
			accauntingDataIdToUpdate = Integer.valueOf(parameters.get("id"));
			loadAccauntingData();

		} else {
			accauntingData = new AccauntingData();
		}
	}

	private void loadAccauntingData() {
		accauntingData = accauntingDataRepository.getById(accauntingDataIdToUpdate);
	}

	public void addAccauntingData() {
		if (accauntingData.getId() == null) {
			accauntingDataRepository.add(accauntingData);
			
		} else {
			accauntingDataRepository.update(accauntingData.getId(), accauntingData);
		}
	}


	public AccauntingData getAccauntingData() {
		return accauntingData;
	}

	public void setAccauntingData(AccauntingData accauntingData) {
		this.accauntingData = accauntingData;
	}
	
}
