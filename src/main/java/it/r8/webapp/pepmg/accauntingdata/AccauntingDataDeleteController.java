package it.r8.webapp.pepmg.accauntingdata;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.inject.Inject;
import it.r8.webapp.pepmg.core.Repository;

@ManagedBean
@ApplicationScoped
public class AccauntingDataDeleteController {
	
	private AccauntingData accauntingData;
	@Inject
	private Repository<Integer,AccauntingData> accauntingDataRepository;
	
	public void deleteAccauntingData(int id) {
		accauntingDataRepository.delete(id);
	}
	
	public AccauntingData getAccauntingData() {
		return accauntingData;
	}
	public void setaccauntingData(AccauntingData accauntingData) {
		this.accauntingData = accauntingData;
	}

}
