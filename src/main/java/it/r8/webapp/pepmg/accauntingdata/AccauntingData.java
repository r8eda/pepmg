package it.r8.webapp.pepmg.accauntingdata;

import java.sql.*;

import it.r8.webapp.pepmg.core.BaseDomain;

public class AccauntingData extends BaseDomain<Integer> {
	
	private static final long serialVersionUID = 1L;

	private int idCompany;
	private int idfreeLancer;
	private double amount;
	private Timestamp orderDate;
	
	public int getIdCompany() {
		return idCompany;
	}
	public void setIdCompany(int idCompany) {
		this.idCompany = idCompany;
	}
	public int getIdfreeLancer() {
		return idfreeLancer;
	}
	public void setIdfreeLancer(int idfreeLancer) {
		this.idfreeLancer = idfreeLancer;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public Timestamp getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(Timestamp orderDate) {
		this.orderDate = orderDate;
	}
	
}
