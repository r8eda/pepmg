package it.r8.webapp.pepmg.freelancer;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.inject.Inject;
import it.r8.webapp.pepmg.core.Repository;

@ManagedBean
@ApplicationScoped
public class FreeLancerDeleteController {
	
	private FreeLancer freeLancer;
	@Inject
	private Repository<Integer,FreeLancer> freeLancerRepository;
	
	public void deleteFreeLancer(int id) {
		freeLancerRepository.delete(id);
	}
	
	public FreeLancer getFreeLancer() {
		return freeLancer;
	}
	public void setUser(FreeLancer freeLancer) {
		this.freeLancer = freeLancer;
	}
}
