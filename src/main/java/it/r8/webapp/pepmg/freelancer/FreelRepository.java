package it.r8.webapp.pepmg.freelancer;

import java.util.List;

import it.r8.webapp.pepmg.freelancer.FreeLancer;
import it.r8.webapp.pepmg.core.Repository;

public interface FreelRepository extends Repository<Integer, FreeLancer> {

	List<FreeLancer> getAllClients();
	
	List<FreeLancer> getAllSuppliers();
	
	public void addClients(FreeLancer freelancer);
	
	public void addSupplier(FreeLancer freelancer);
}
