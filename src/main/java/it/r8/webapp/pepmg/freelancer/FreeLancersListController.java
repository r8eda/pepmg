package it.r8.webapp.pepmg.freelancer;

import java.util.List;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.inject.Inject;

import it.r8.webapp.pepmg.core.Repository;

@ManagedBean
@ApplicationScoped
public class FreeLancersListController {

	private String test;
	
	@Inject
	private Repository<Integer,FreeLancer> freeLancerRepository;

	public String getTest() {
		return test;
	}

	public void setTest(String test) {
		this.test = test;
	}

	public List<FreeLancer> getFreeLancers() {
		return freeLancerRepository.getAll();
	}
}
