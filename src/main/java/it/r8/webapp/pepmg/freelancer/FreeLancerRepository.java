package it.r8.webapp.pepmg.freelancer;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Singleton;
import javax.enterprise.context.Dependent;

import it.r8.webapp.pepmg.core.*;


@Dependent
@Singleton
public class FreeLancerRepository extends DbConnection implements Repository<Integer, FreeLancer>  { 

	public void add(FreeLancer freeLancer) {
		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			String sql = String.format("INSERT INTO freelancer (name, surname, fiscalCode, iva, email, telephone, city, address, district) " + 
										  "VALUES ('%s', '%s', '%s','%s','%s','%s','%s','%s', '%s')",freeLancer.getName(),
									    freeLancer.getSurname(), freeLancer.getFiscalCode(),freeLancer.getIva(), freeLancer.getEmail(), freeLancer.getTelephone(), freeLancer.getCity(), freeLancer.getAddress(), freeLancer.getDistrict());
			statement.executeUpdate(sql);

		} catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
		}
	}

	public List<FreeLancer> getAll() {
		List<FreeLancer> freeLancers = new ArrayList<FreeLancer>();

		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT * FROM freelancer");

			while (resultSet.next()) {
				FreeLancer freeLancer = new FreeLancer();
				freeLancer.setId(resultSet.getInt("id"));
				freeLancer.setName(resultSet.getString("name"));
				freeLancer.setSurname(resultSet.getString("surname"));
				freeLancer.setFiscalCode(resultSet.getString("fiscalCode"));
				freeLancer.setIva(resultSet.getString("iva"));
				freeLancer.setEmail(resultSet.getString("email"));
				freeLancer.setTelephone(resultSet.getString("telephone"));
				freeLancer.setAddress(resultSet.getString("address"));
				freeLancer.setCity(resultSet.getString("city"));
				freeLancer.setDistrict(resultSet.getString("district"));

				freeLancers.add(freeLancer);
			}

		} catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
		}

		return freeLancers;
	}
	
	public void update(Integer id, FreeLancer freeLancer) {
		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			String sql = String.format("UPDATE freelancer f" +
			                           "  SET name = '%s'," +
									   "	  surname = '%s'  ," +
									   "      fiscalCode = '%s' ," +
									   "      iva =  '%s' ," +
									   "	  email = '%s'  ," +
									   "	  telephone = '%s' , " +
									   "      address = '%s' , " +
									   "      city = '%s', " +
									   "      district = '%s' " +
									   "WHERE f.id = %d", freeLancer.getName(),
									   freeLancer.getSurname(), freeLancer.getFiscalCode(),freeLancer.getIva(),freeLancer.getEmail(), freeLancer.getTelephone(), freeLancer.getAddress(), freeLancer.getCity(), freeLancer.getDistrict(), id);
			
			statement.executeUpdate(sql);
			
		} catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
		}
	}
	
	public void delete(Integer id) {
		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			String sql = String.format("DELETE FROM freelancer WHERE id = %d", id);
			
			int result = statement.executeUpdate(sql);
			System.out.println(result);
			
		} catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
		}
	}
	
	public FreeLancer getById(Integer id) {
		FreeLancer freeLancer = null;

		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			String sql = String.format("SELECT * FROM freelancer f WHERE f.id = '%s'", id);
			
			ResultSet resultSet = statement.executeQuery(sql);
			
			if (resultSet.first()) {
				freeLancer = new FreeLancer();
				freeLancer.setId(resultSet.getInt("id"));
				freeLancer.setName(resultSet.getString("name"));
				freeLancer.setSurname(resultSet.getString("surname"));
				freeLancer.setFiscalCode(resultSet.getString("fiscalCode"));
				freeLancer.setIva(resultSet.getString("iva"));
				freeLancer.setEmail(resultSet.getString("email"));
				freeLancer.setTelephone(resultSet.getString("telephone"));
				freeLancer.setAddress(resultSet.getString("address"));
				freeLancer.setCity(resultSet.getString("city"));
				freeLancer.setDistrict(resultSet.getString("district"));
				
			}
			
		} catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
		}

		return freeLancer;
	}

}