package it.r8.webapp.pepmg.freelancer;

import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import it.r8.webapp.pepmg.core.Repository;

@ManagedBean
@RequestScoped
public class FreeLancerAddController {

	private FreeLancer freeLancer;

	private Integer freeLancerIdToUpdate;

	@Inject
	private Repository<Integer, FreeLancer> freeLancerRepository;

	@PostConstruct
	public void init() {
		Map<String, String> parameters = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();


		if (parameters.get("id") != null) {
			freeLancerIdToUpdate = Integer.valueOf(parameters.get("id"));
			loadFreeLancer();

		} else {
			freeLancer = new FreeLancer();
		}
	}

	private void loadFreeLancer() {
		freeLancer = freeLancerRepository.getById(freeLancerIdToUpdate);
	}

	public void addFreeLancer() {
		if (freeLancer.getId() == null) {
			freeLancerRepository.add(freeLancer);
			
		} else {
			freeLancerRepository.update(freeLancer.getId(), freeLancer);
		}
	}


	public FreeLancer getFreeLancer() {
		return freeLancer;
	}

	public void setFreeLancer(FreeLancer freeLancer) {
		this.freeLancer = freeLancer;
	}
}
