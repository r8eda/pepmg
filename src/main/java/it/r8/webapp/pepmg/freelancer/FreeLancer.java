package it.r8.webapp.pepmg.freelancer;

import javax.validation.constraints.Size;

import it.r8.webapp.pepmg.core.BaseDomain;
import it.r8.webapp.pepmg.user.validators.Username;

public class FreeLancer extends BaseDomain<Integer> {

	private static final long serialVersionUID = 1L;
	
	private boolean supplier;
	@Size(min = 3, max = 20, message = "La lunghezza deve essere compresa tra 3 e 20")
	private String name;
	@Size(min = 2, max = 20, message = "La lunghezza deve essere compresa tra 2 e 20")
	private String surname;
	@Size(min =16, max =16, message = "il codice fiscale deve essere di 16 caratteri")
	private String fiscalCode;
	@Size (min = 11, max = 11, message = "La lunghezza deve essere  di 11 caratteri")
	private String iva;
	@Size (min = 9, max = 15, message = "La lunghezza deve essere compresa tra 9 e 15")
	private String telephone;
	@Username
	private String email;
	private String address;
	private String city;
	private String district;
	public boolean isSupplier() {
		return supplier;
	}
	public void setSupplier(boolean supplier) {
		this.supplier = supplier;	
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getFiscalCode() {
		return fiscalCode;
	}
	public void setFiscalCode(String fiscalCode) {
		this.fiscalCode = fiscalCode;
	}
	public String getIva() {
		return iva;
	}
	public void setIva(String iva) {
		this.iva = iva;
	}
	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getDistrict() {
		return district;
	}
	public void setDistrict(String district) {
		this.district = district;
	}	
	
}
