package it.r8.webapp.pepmg.company;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.inject.Inject;

import it.r8.webapp.pepmg.core.BaseController;
import it.r8.webapp.pepmg.core.Repository;

@ManagedBean
@RequestScoped
public class CompanyListController extends BaseController {

	private Integer typeCompany;
	private List<Company> companies;
	
	@Inject
	private CompRepository repository;
	
	@Override
	public void init() {
		typeCompany = parameters.containsKey("typeCompany") ? 
				Integer.valueOf(parameters.get("typeCompany")) : 1;
				
		if (typeCompany ==0) {
			companies = repository.getAllClients();
			
		} else {
			companies = repository.getAllSuppliers();
		}
	}

	public Integer getTypeCompany() {
		return typeCompany;
	}


	public void setTypeCompany(Integer typeCompany) {
		this.typeCompany = typeCompany;
	}

	public List<Company> getCompanies() {
		return companies;
	}

	public void setCompanies(List<Company> companies) {
		this.companies = companies;
	}
}