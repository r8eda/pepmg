package it.r8.webapp.pepmg.company;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.inject.Inject;
import it.r8.webapp.pepmg.core.Repository;

@ManagedBean
@ApplicationScoped
public class CompanyDeleteController {
	
	private Company company;
	@Inject
	private Repository<Integer,Company> companyRepository;
	
	public void deleteCompany(int id) {
		companyRepository.delete(id);
	}
	
	public Company getCompany() {
		return company;
	}
	public void setUser(Company company) {
		this.company = company;
	}
}
