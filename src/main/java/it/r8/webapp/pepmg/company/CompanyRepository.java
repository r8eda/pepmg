package it.r8.webapp.pepmg.company;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Singleton;
import javax.enterprise.context.Dependent;

import it.r8.webapp.pepmg.core.*;

@Dependent
@Singleton
public class CompanyRepository extends DbConnection implements CompRepository {
		
	public void add(Company company) {
		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)){
	    	Statement statement = connection.createStatement();
			String sql = String.format("INSERT INTO company (businessName, email, telephone, iva, ceo, projectManager, legalAddress, headquarter, area, category, qualified) " + 
											  "VALUES ('%s', '%s', '%s','%s','%s','%s','%s','%s', '%s', '%s', '%s')", company.getBusinessName(),
										      company.getEmail(), company.getTelephone(),company.getIva(), company.getCeo(), company.getProjectManager(), company.getLegalAddress(),
										      company.getHeadQuarter(), company.getArea(), company.getCategory(), company.getQualified());
			statement.executeUpdate(sql);
	    } catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
	    }
	}
	

	public void addSupplier(Company company) {
		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)){
	    	Statement statement = connection.createStatement();
			String sql = String.format("INSERT INTO company (supplier, businessName, email, telephone, iva, ceo, projectManager, legalAddress, headquarter, area, category, qualified) " + 
											  "VALUES (1, '%s', '%s', '%s','%s','%s','%s','%s','%s', '%s', '%s', '%s')", company.getSupplier(), company.getBusinessName(),
										      company.getEmail(), company.getTelephone(),company.getIva(), company.getCeo(), company.getProjectManager(), company.getLegalAddress(),
										      company.getHeadQuarter(), company.getArea(), company.getCategory(), company.getQualified());
			statement.executeUpdate(sql);
	    } catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
	    }
	}
	public void addClient(Company company) {
		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)){
	    	Statement statement = connection.createStatement();
			String sql = String.format("INSERT INTO company (supplier, businessName, email, telephone, iva, ceo, projectManager, legalAddress, headquarter, area, category, qualified) " + 
											  "VALUES (0, '%s', '%s', '%s','%s','%s','%s','%s','%s', '%s', '%s', '%s')", company.getSupplier(), company.getBusinessName(),
										      company.getEmail(), company.getTelephone(),company.getIva(), company.getCeo(), company.getProjectManager(), company.getLegalAddress(),
										      company.getHeadQuarter(), company.getArea(), company.getCategory(), company.getQualified());
			statement.executeUpdate(sql);
	    } catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
	    }
	}
	
	
	public List<Company> getAll() {
		List<Company> companys = new ArrayList<Company>();

	  	try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT * FROM company");
	
	  		while (resultSet.next()) {
				Company company = new Company();
				company.setId(resultSet.getInt("id"));
				company.setBusinessName(resultSet.getString("businessName"));
		        company.setEmail(resultSet.getString("email"));
				company.setTelephone(resultSet.getString("telephone"));
				company.setIva(resultSet.getString("iva"));
				company.setCeo(resultSet.getString("ceo"));
				company.setProjectManager(resultSet.getString("projectManager"));
				company.setLegalAddress(resultSet.getString("legalAddress"));
				company.setHeadQuarter(resultSet.getString("headquarter"));
				company.setArea(resultSet.getString("area"));
				company.setCategory(resultSet.getString("category"));
				company.setQualified(resultSet.getString("qualified"));

				companys.add(company);
			}
      	} catch (SQLException e) {
      		System.out.println("SQL exception");
      		e.printStackTrace();
      	}
	  	return companys;
	}
	
	public List<Company> getAllClients() {
		List<Company> companys = new ArrayList<Company>();

	  	try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT * FROM company AS c WHERE c.supplier = 0");
	
	  		while (resultSet.next()) {
				Company company = new Company();
				company.setId(resultSet.getInt("id"));
				company.setBusinessName(resultSet.getString("businessName"));
		        company.setEmail(resultSet.getString("email"));
				company.setTelephone(resultSet.getString("telephone"));
				company.setIva(resultSet.getString("iva"));
				company.setCeo(resultSet.getString("ceo"));
				company.setProjectManager(resultSet.getString("projectManager"));
				company.setLegalAddress(resultSet.getString("legalAddress"));
				company.setHeadQuarter(resultSet.getString("headquarter"));
				company.setArea(resultSet.getString("area"));
				company.setCategory(resultSet.getString("category"));
				company.setQualified(resultSet.getString("qualified"));

				companys.add(company);
			}
      	} catch (SQLException e) {
      		System.out.println("SQL exception");
      		e.printStackTrace();
      	}
	  	return companys;
	}
	
	public List<Company> getAllSuppliers() {
		List<Company> companys = new ArrayList<Company>();

	  	try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT * FROM company AS c WHERE c.supplier = 1");
	
	  		while (resultSet.next()) {
				Company company = new Company();
				company.setId(resultSet.getInt("id"));
				company.setBusinessName(resultSet.getString("businessName"));
		        company.setEmail(resultSet.getString("email"));
				company.setTelephone(resultSet.getString("telephone"));
				company.setIva(resultSet.getString("iva"));
				company.setCeo(resultSet.getString("ceo"));
				company.setProjectManager(resultSet.getString("projectManager"));
				company.setLegalAddress(resultSet.getString("legalAddress"));
				company.setHeadQuarter(resultSet.getString("headquarter"));
				company.setArea(resultSet.getString("area"));
				company.setCategory(resultSet.getString("category"));
				company.setQualified(resultSet.getString("qualified"));

				companys.add(company);
			}
      	} catch (SQLException e) {
      		System.out.println("SQL exception");
      		e.printStackTrace();
      	}
	  	return companys;
	}
	
	public void update(Integer id, Company company) {
		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			String sql = String.format("UPDATE company c" +
									   "  SET businessName = '%s'," +						   
									   "      email = '%s'  ," +
									   "	  telephone = '%s' , " +
									   "      iva =  '%s' ," +
									   "      ceo =  '%s' ," +
									   "      projectManager = '%s' , " +
									   "      legalAddress = '%s' , " +
									   "      headquarter = '%s' , " +
									   "	  area = '%s'," +
									   "      category = '%s'," +
									   "      qualified = '%s' " +
									   "WHERE c.id = %d", company.getBusinessName(),
									   company.getEmail(), company.getTelephone(),company.getIva(),company.getCeo(), company.getProjectManager(), 
									   company.getLegalAddress(), company.getHeadQuarter(), company.getArea(), company.getCategory(), company.getQualified(), id);
			statement.executeUpdate(sql);
		
		} 
		catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
		}	
	}

	public void delete(Integer id) {
		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			String sql = String.format("DELETE FROM company WHERE id = %d", id);
				
			int result = statement.executeUpdate(sql);
			System.out.println(result);
				
		} catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
		}
	}
		
	public Company getById(Integer id) {
		Company company = null;

		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			String sql = String.format("SELECT * FROM company c WHERE c.id = '%s'", id);
				
			ResultSet resultSet = statement.executeQuery(sql);
	
			if (resultSet.first()) {
				company = new Company();
				company.setId(resultSet.getInt("id"));
				company.setBusinessName(resultSet.getString("businessName"));
		        company.setEmail(resultSet.getString("email"));
				company.setTelephone(resultSet.getString("telephone"));
				company.setIva(resultSet.getString("iva"));
				company.setCeo(resultSet.getString("ceo"));
				company.setProjectManager(resultSet.getString("projectManager"));
				company.setLegalAddress(resultSet.getString("legalAddress"));
				company.setHeadQuarter(resultSet.getString("headquarter"));
				company.setArea(resultSet.getString("area"));
				company.setCategory(resultSet.getString("category"));
				company.setQualified(resultSet.getString("qualified"));
			}
			
		} catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
		}

		return company;
	}

}
	