package it.r8.webapp.pepmg.company;

import javax.validation.constraints.Size;

import it.r8.webapp.pepmg.core.BaseDomain;
import it.r8.webapp.pepmg.user.validators.Username;

public class Company extends BaseDomain<Integer> {
	
	private static final long serialVersionUID = 1L;
	
	private int supplier;
	
	@Size(min = 3, max = 20, message = "La lunghezza deve essere compresa tra 3 e 20")
	private String businessName;
    @Username
	private String email;
	@Size (min = 9, max = 15, message = "La lunghezza deve essere compresa tra 9 e 15")
	private String telephone;
	@Size (min = 11, max = 11, message = "La lunghezza deve essere  di 11 caratteri")
	private String iva;
	private String ceo;
	private String projectManager;
	private String legalAddress;
	private String headQuarter;
	private String area;
	private String category;
	private String qualified;
	
	public int getSupplier() {
		return supplier;
	}
	public void setSupplier(int supplier) {
		this.supplier = supplier;
	}
	public String getBusinessName() {
		return businessName;
	}
	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	public String getIva() {
		return iva;
	}
	public void setIva(String iva) {
		this.iva = iva;
	}
	public String getCeo() {
		return ceo;
	}
	public void setCeo(String ceo) {
		this.ceo = ceo;
	}
	public String getProjectManager() {
		return projectManager;
	}
	public void setProjectManager(String projectManager) {
		this.projectManager = projectManager;
	}
	public String getLegalAddress() {
		return legalAddress;
	}
	public void setLegalAddress(String legalAddress) {
		this.legalAddress = legalAddress;
	}
	public String getHeadQuarter() {
		return headQuarter;
	}
	public void setHeadQuarter(String headQuarter) {
		this.headQuarter = headQuarter;
	}
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getQualified() {
		return qualified;
	}
	public void setQualified(String qualified) {
		this.qualified = qualified;
	}
	
}
