package it.r8.webapp.pepmg.company;

import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import it.r8.webapp.pepmg.core.Repository;

@ManagedBean
@RequestScoped
public class CompanyAddController {

	private Company company;
	private Integer companyIdToUpdate;
	private Integer typeCompany;

	@Inject
	private Repository<Integer, Company> companyRepository;
	@Inject
	private CompRepository repository;
	
	@PostConstruct
	public void init() {
		Map<String, String> parameters = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();

		typeCompany = parameters.containsKey("typeCompany") ? 
				Integer.valueOf(parameters.get("typeCompany")) : 0;

		if (parameters.get("id") != null) {
			companyIdToUpdate = Integer.valueOf(parameters.get("id"));
			loadCompany();

		} else {
			company = new Company();
		}
	}

	private void loadCompany() {
		company = companyRepository.getById(companyIdToUpdate);
	}

	public void addCompany() {
		
		
		if (company.getId() == null) {
			if (typeCompany == 0) {
				repository.addClient(company);
				
			} else {
				repository.addSupplier(company);
				
			}
			
			
		} else {
			companyRepository.update(company.getId(), company);
		}
	}


	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}
	
	public Integer getTypeCompany() {
		return typeCompany;
	}

	public void setTypeCompany(Integer typeCompany) {
		this.typeCompany = typeCompany;
	}
}
