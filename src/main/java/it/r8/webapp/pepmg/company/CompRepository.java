package it.r8.webapp.pepmg.company;

import java.util.List;

import it.r8.webapp.pepmg.core.Repository;

public interface CompRepository extends Repository<Integer, Company> {

	List<Company> getAllClients();
	
	List<Company> getAllSuppliers();
	
	public void addClient(Company company);
	
	public void addSupplier(Company company);
}
