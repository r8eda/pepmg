package it.r8.webapp.pepmg.core;

public abstract class DbConnection {
	
	protected static final String URL = "jdbc:mysql://localhost:3306/pepmg?serverTimezone=UTC&autoReconnect=true&useSSL=false";
	protected static final String USER = "root";
	protected static final String PASSWORD = "test";

}
