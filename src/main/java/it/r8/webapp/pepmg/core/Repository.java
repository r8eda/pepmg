package it.r8.webapp.pepmg.core;

import java.util.List;
import java.io.Serializable;

public interface Repository<ID extends Serializable, D extends BaseDomain<ID>> {

	public void add(D entity);
	
	public List<D> getAll();
	
	public void update(ID id, D entity); 
	
	public void delete (ID id);
	
	public D getById(ID id);
}
