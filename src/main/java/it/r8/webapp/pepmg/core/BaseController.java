package it.r8.webapp.pepmg.core;

import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;

public abstract class BaseController {
	
	protected Map<String, String> parameters;
	
	@PostConstruct
	public void postConstruct() {
		parameters = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		init();
	}
	
	public abstract void init();
}
