package it.r8.webapp.pepmg.core;

import java.io.Serializable;

public class BaseDomain<ID extends Serializable> implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private ID id;

	public ID getId() {
		return id;
	}

	public void setId(ID id) {
		this.id = id;
	}
}
