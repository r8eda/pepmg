![enter image description here](img/logo-protom.png)

**Documento di analisi e progettazione Software**
[PePMG](https://bitbucket.org/r8eda/pepmg)

**Indice**

1.  Introduzione

2.  Glossario

3.  Funzionalit� ed utenti

4.  Requisiti di funzionalit�

5.  Requisiti non funzionali

6.  Design Pattern

7.  Evoluzione del sistema

8.  Strumenti di sviluppo

**1. Introduzione**

Lo scopo di questo documento � descrivere cosa il progetto dovr� offrire e di
esplicare l'utilizzo di particolari strumenti adatti per l'analisi,
progettazione, implementazione e test del software.

Il software � stato commissionato da Project & Planning. La richiesta � stata
quella di realizzare un programma applicativo e di creare un archivio
strutturato in modo tale da consentire la gestione dei dati stessi (inserimento,
ricerca, cancellazione e loro aggiornamento). � stato richiesto che il programma
fosse scritto in Java per migliorare la portabilit�, che il codice fosse solido
e robusto in grado di gestire dati in ingresso anche se inattesi e reagire
all'errore senza eccessi e che inoltre ci fosse uno specifico riferimento per le
commesse assegnando il codice 01 per le consulenze , codice 02 per la
formazione, un progressivo per il numero di commessa , le ultime due cifre
dell'anno di riferimento della commessa e l'acronimo AP se riferito ad azienda
privata e PA se riferita a pubblica amministrazione (ad esempio 010118AP :
consulenza, prima commessa, anno 2018, azienda privata ) .

**2. Glossario**

**UML (***Unified Modeling Language*, "linguaggio di modellazione unificato")
viene usato per descrivere il dominio applicativo e il comportamento del sistema
software, consideremo alcuni diagrammi dello standard UML con lo scopo di
agevolare e dare sostegno in fase di analisi sia agli analisti, ai clienti e ai
fornitori (Class Diagram, Use Case Diagram).

**Class Diagram** viene usato per descrivere tipi di entit�, con le loro
caratteristiche, funzioni e le eventuali relazioni fra questi tipi.

**Use Case Diagram** viene usato per la descrizione delle funzioni o servizi
offerti dal sistema, cos� come percepiti e utilizzati dagli attori che
interagiscono col sistema stesso.

**http** viene usato come principale sistema per la trasmissione di informazioni
sul web.

**Query** viene usata per interrogare un database per raccogliere informazioni.

**Bug** viene usato per indicare un errore di funzionamento del software.

**Crash** viene usato per indicare l'improvvisa chiusura o blocco completo
dell'intero software.

**XML** viene usato per scambiare informazioni e per includere tag specificati
dall'azienda per il determinato software.

**3. Funzionalit� ed utenti**

**3.1 Amministratore**

L'amministratore (admin) avr� il controllo totale della gestione del database,
ha la possibilit� di visualizzare, aggiungere, modificare ed eliminare tutti i
dati dei fornitori e dei clienti. Infine l'amministratore ha la possibilit� di
scegliere eventuali collaboratori per mantenere e aggiornare l'intero sistema.

**3.2 Collaboratori di admin**

I collaboratori vengono scelti dall'admin e ottengono dei privilegi che
comportano l'accesso ad alcune sezioni dell'app con la possibilit� di aggiornare
e mantenere il sistema software.

**3.3 Fornitori**

I fornitori sono quei soggetti economici che forniscono alla Project & Planning
dei servizi, delle soluzioni, delle conoscenze e delle professionalit�. I
fornitori o consulenti esterni offrono tali servizi attraverso societ� o come
liberi professionisti.

**3.4 Clienti**

La Project & Planning svolge attivit� di supporto a quella dell'impresa cliente
o del soggetto singolo cliente . Tale attivit� si pu� esprimere attraverso
tipologie di intervento progettuale e di supporto.

**4. Requisiti di funzionalit�**

Consiste in ogni informazione circa le funzionalit�, i servizi, le modalit�
operative e di gestione del sistema

da sviluppare.

*Class Diagram (UML)*

![enter image description here](img/Classdiagram.jpg)

*Use Case Diagram (UML)*

![enter image description here](img/Usecases.jpg)

**4.1 Accesso ad un sistema di gestione degli account**

**Attori**: Amministratore

**Dettagli**: il sistema dovr� fornire un�interfaccia di accesso per la gestione
degli account del sistema interno.

**Motivazione**: permettere all�amministratore di gestire gli utenti.

**4.2 Creazione di nuovi account**

**Attori**: Amministratore

**Dettagli**: il sistema dovr� fornire la possibilit� all�amministratore di
poter creare nuovi account con diversi propriet� d�accesso per ogni utente .

**Motivazione**: permettere all�amministratore di gestire gli utenti.

**4.3 Modifica account**

**Attori**: Amministratore

**Dettagli**: il sistema dovr� fornire la possibilit� all�amministratore di
poter modificare gli account.

**Motivazione**: permettere all�amministratore di modificare attributi,
privilegi e caratteristiche degli utenti in caso di cambiamenti.

**4.4 Cancellazione account**

**Attori**: Amministratore

**Dettagli**: il sistema dovr� fornire la possibilit� all�amministratore di
poter cancellare eventualmente gli account.

**Motivazione**: permettere all�amministratore di cancellare uno o pi� utenti.

**4.5 Aggregazione dati**

**Attori**: Amministratore, Collaboratori

**Dettagli**: il sistema dovr� fornire la possibilit� di raccogliere diverse
tipologie di dato ma dovr� uniformarli per poi poterli aggregare

**Motivazione**: al fine di analizzare e confrontare pi� dati distinti si avr�
la necessit� di aggregare dati secondo criteri preattribuiti.

**4.6 Compilazione scheda contabile**

**Attori**: Amministratore, Collaboratori

**Dettagli**: il sistema dovr� permettere di allegare una scheda contabile
riferita ad un anno specifico.

**Motivazione**: L'amministratore e i suoi collaboratori dovranno avere la
possibilit� di visionare la scheda contabile.

**4.7 Compilazione query**

**Attori**: Analista

**Dettagli**: il sistema dovr� permettere l�inserimento e la memorizzazione di
apposite query.

**Motivazione**: l�analista dovr� avere la possibilit� di creare query
personalizzate.

**4.8 Compilazione query**

**Attori**: Analista

**Dettagli**: il sistema dovr� permettere l�inserimento e la memorizzazione di
apposite query.

**Motivazione**: l�analista dovr� avere la possibilit� di creare query
personalizzate.

**4.9 Modifica/Eliminazione query**

**Attori**: Analista

**Dettagli**: il sistema dovr� permettere la modifica e l�eliminazione di query
memorizzate nel database.

**Motivazione**: l�analista dovr� avere la possibilit� di modificare e
cancellare query personalizzate.

**4.10 Visualizzazione risultato della query**

**Attori**: Analista

**Dettagli**: il sistema dovr� permettere all�analista di poter consultare il
risultato di una query

**Motivazione**: per poter interpretare al meglio i risultati della campagna
raccolta dati, l�analista dovr� appunto avere la possibilit� di visualizzare
liberamente i singoli risultati.

**4.11 Inoltro dei questionari a fornitori e clienti**

**Attori**: Amministratore, Collaboratori

**Dettagli**: il sistema permetter� all'amministratore o ai suoi collaboratori
di poter inoltrare i questionari ad un set di destinatari predefinito.

**Motivazione**: una volta creato il questionario potr� essere inoltrato al
numero pi� ampio di utenti in modo agevole da parte dell'amministratore.

**4.12 Definizione privilegi account**

**Attori:** Amministratore

**Dettagli:** il sistema dovr� fornire la possibilit� di definire livelli di
accesso per i diversi attori o utilizzatori del servizio offerto con la
possibilit� di scegliere utenti con privilegi di collaboratori.

**Motivazione:** il gestore del sistema si avvale dell'assistenza di
collaboratori per rendere pi� efficienti le raccolte dati .

**5. Requisiti non funzionali**

I requisiti non funzionali non sono collegati direttamente con le funzioni
implementate dal sistema, ma piuttosto alle modalit� operative e di gestione. Di
seguito si evidenzieranno i vincoli a

cui il sistema si dovr� attenere.

**5.1 Requisiti di usabilit�**

Il sistema deve essere consultabile dall'amministratore

senza nessuna particolare difficolt� e compreso perfettamente per l'utilizzo
stesso della piattaforma e dei database.

**5.2 Requisiti di efficienza**

Il sistema deve poter fornire un�ottima efficienza determinata dalla velocit� di
banda e dell�apparecchiatura hardware a disposizione. Il sistema deve rispondere
ad ogni comando dell'utilizzatore entro pochi secondi.

**5.3 Requisiti di affidabilit�**

Il sistema non deve consentire il verificarsi di errori critici, cio� di quelle
tipologie di errori che comportano la perdita di dati, o perlomeno limitarne
considerevolmente la probabilit�. L�acquisizione dei dati deve essere robusta e
sicura, in particolare dovr� essere garantita la corretta esecuzione delle
raccolta dati, informando tempestivamente l�amministratore di sistema in caso di
anomalie o guasti.

**6. Design Pattern**

Il software � stato progettato utilizzando il design pattern di creazione
Singleton strutturando le classi in modo tale da ottenere determinate
caratteristiche implementative per aggiungere particolari funzionalit� al
programma.

**7. Evoluzione del sistema**

Il software ha diverse prospettive di sviluppo, al termine del progetto si
andranno ad elencare i possibili cambiamenti e migliorie adattabili al sistema.

**8. Strumenti di sviluppo**

Il progetto � stato sviluppato utilizzando i seguenti strumenti di sviluppo

-   Eclipse Java EE IDE for Web Developers

-   MySQL Workbench 6.3

-   GitHub

-   StarUML
